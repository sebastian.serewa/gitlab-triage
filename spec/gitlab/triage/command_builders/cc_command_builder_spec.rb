require 'spec_helper'

require 'gitlab/triage/command_builders/cc_command_builder'

describe Gitlab::Triage::CommandBuilders::CcCommandBuilder do
  let(:usernames) do
    %w[keith joe_bloggs]
  end

  describe '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(usernames)
      expect(builder.build_command).to eq("/cc @keith @joe_bloggs")
    end

    it 'outputs an empty string if no usernames present' do
      builder = described_class.new([])
      expect(builder.build_command).to eq("")
    end
  end
end
