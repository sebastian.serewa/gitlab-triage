# frozen_string_literal: true

require 'spec_helper'

describe 'unknown label' do
  include_context 'with integration context'

  it 'comments on the issue' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              actions:
                labels: ['foo']
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end

    stub_get = stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/labels/foo",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      []
    end

    expect { perform(rule) }.to raise_error(Gitlab::Triage::Resource::Label::LabelDoesntExistError)

    assert_requested(stub_get)
  end
end
